|Name |Tea                                              |Coffee        |
|:----|:----                                            |:----         |
|Sam  |Milk, 1/3 spoon of Truvia.                       | -            |
|James|Milk, 1/2 sugar                                  |Black, 1 sugar|
|Dave |http://live.dbpedia.org/resource/Builder%27s_tea | -            |
|Stu  |Milk, no sugar						             |White, 1 sugar|
|Ulrich  |Green tea, no sugar		                     |Espresso, black|
|Jeni |Earl Grey, weak and lots of milk but squeeze the tea bag|Large & black|
